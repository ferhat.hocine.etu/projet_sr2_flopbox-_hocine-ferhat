#SR2_PROJET_FlopBox 
#Ferhat_HOCINE_M1GL


## 1- Intoduction

L'objectif du tp est de développer la plate-forme FlopBox en adoptant le style architectural REST pour vous permettre de centraliser la gestion de vos fichiers distants stockés dans des serveurs FTP tiers.

## 2-Exécution:

Dans le fichier racine on exécute la commande :
->>> mvn clean compile    
//pour compliler le projet

->>> mvn exec:java  
//pour éxécuter le projet

exemple:

->>>curl -X POST -H "url: ftp.ubuntu.com" -H "user: anonymous" -H "password: anonymous" http://localhost:8080/FlopBox/server?alias=ftp
//pour la connexion


->>>curl -X GET http://localhost:8080/FlopBox/server/list/cdimage
//pour avoir la liste des sous élements d'un dossier

->>>curl -X GET http://localhost:8080/FlopBox/listServers/servers
//pour avoir la lister de nous servers

->>>curl -X GET http://localhost:8080/FlopBox/listServers/getServer?alias=ftp
//pour avoir l'Adresse d'un serveur en fournissant l'alias

->>>curl -X POST   'http://localhost:8080/FlopBox/listServers/add?alias=ftp2&url=www.free.fr'
//pour ajouter un nouveau serveur

->>>curl -X PUT  'http://localhost:8080/FlopBox/listServers/Put?alias=ftp2&url_update=www.free2.fr'
//pour modifier les information d'un serveur

->>>curl -X curl -X DELETE  'http://localhost:8080/FlopBox/listServers/delete?alias=ftp2'
//pour supprimer un serveur


## 3-Architecture
a)-Gestion d'erreur :
les erreurs sont gérées dans les réponses renvoyé par chaque méthode.
exemple:

'''

    /**
     * URL http://localhost:8080/FlopBox/listServers/getServer?alias=ftp
     * function that return the server associated to an alias in the list of servers
     * @param alias
     * @return Response
     */
    @GET
    @Path("/getServer")
    public Response getServer(@QueryParam("alias") String alias){
    	if((alias== null)) {
        	return Response.status(Response.Status.BAD_REQUEST).entity("enter the parameter alias  \n").build();
    	}
    	for (int i=0; i<Main.list_alias.size();i++) {
    		if(Main.list_alias.get(i).getAlis().equals(alias)){
    	        return Response.status(Response.Status.OK).entity("Server : "+Main.list_alias.get(i).getUrl()+"  \n").build();
    		}
    	}
        return Response.status(Response.Status.BAD_REQUEST).entity("Server not Found  \n").build();
    }
    
'''

dans cette exemple l'erreur du paramètre manquant est gérée par le 1er retour de response.
l'erreur de paramètre introuvable est gérée par le 3éme retour de réponse.
dans le cas ou il ne y'a pas d'erreur le 2éme retour nous retourne le résultat voulu.

try - catch

'''

    /**
     * function that connect to server
     * @param User user
     * @param Alias al
     * @return Response
     * @throws IOException
     */
    public Response connect(User user,Alias al ) throws IOException {

        ftpClient = new FTPClient();

        try {
            ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out), true));
            ftpClient.connect(al.getUrl());
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                ftpClient.disconnect();
                return Response.status(Response.Status.BAD_REQUEST).entity("connection failed error (alias url).  \n").build();    
            }
            ftpClient.login(user.getIdent(), user.getPassword());
            replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                ftpClient.disconnect();
                return Response.status(Response.Status.BAD_REQUEST).entity("connection failed error (user,password).  \n").build();    
            }
            ftpClient.enterLocalPassiveMode();
        	}
        catch(IOException e){
        	e.printStackTrace();
        }
		Main.Connected_User();
		ServerConnection_user=user;
		ServerConnection_alias=al;
        return Response.status(Response.Status.ACCEPTED).entity("connection succeed.  \n").build();
        }
        
'''

dans cette méthode j'ai utilisé le try catch afin de capturé les erreur liée a la connexion en d'hors des erreur d'utilisateur, mot de passe et server qui sont gérés des les réponses.



## 4-Parcours du code (code samples):

a)resources.Servers.java

'''

    /**
     * URL http://localhost:8080/FlopBox/listServers/servers
     * function that return the list of servers
     * @return Response
     */
    @GET
    @Path("/servers")
    public Response getServers(){
    	String res="";
    	for (int i=0; i<Main.list_alias.size();i++) {
    		res= res + Main.list_alias.get(i).getUrl()+ " ,";
    	}
    	if (res.equals("")) {
            return Response.status(Response.Status.OK).entity("the list of servers is empty   \n").build();
    	}
    	res =res.substring(0, res.length()-1);
    	res = "Server's list = [ "+res+" ]";
        return Response.status(Response.Status.OK).entity(res+"  \n").build();
    }
    
'''

la méthode getServers() retourn la liste des serveur contenu dans la ArrayList(list_alias) du Main.java.
pour commencer cette méthode parcours la list est ajoute ces élèment au String res pour les afficher à la fin.
si la liste est vide l'erreur est géré par la réponse ("the list of servers is empty").


b)resources.Servers.java

'''

    /**
     * URL http://localhost:8080/FlopBox/listServers/add?alias=ftp2&url=www.free.fr
     * function that add a new server to the list of servers
     * @param alias
     * @param server
     * @return Response
     */
    @POST
    @Path("/add")
    public Response AddServers(@QueryParam("alias") String alias, @QueryParam("url") String url){
    	if((alias== null)|| (url== null)) {
        	return Response.status(Response.Status.BAD_REQUEST).entity("enter the two parameters  \n").build();
    	}
    	Alias al=new Alias(alias,url);
    	if (Main.list_alias.contains(al)) {
        	return Response.status(Response.Status.BAD_REQUEST).entity("Server already existe in the list of servers  \n").build();
    	}
    	Main.list_alias.add(al);
        return Response.status(Response.Status.OK).entity("Server added successfuly  \n").build();
    }
    
'''

cette méthode ajoute un server à la liste des serveurs contenu dans Main.java.
pour commencer cette méthode vérifier que les paramètre ne sont pas null puis l'existance  de ce server dans la liste.
si le server existe alors ("Server already existe in the list of servers ") est retourné dans la reponse sinon le server est ajouté et ("Server added successfuly") est retourné.


c)resources.ServerConnection.java


'''

    /**
     * function that connect to server
     * @param User user
     * @param Alias al
     * @return Response
     * @throws IOException
     */
    public Response connect(User user,Alias al ) throws IOException {

        ftpClient = new FTPClient();

        try {
            ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out), true));
            ftpClient.connect(al.getUrl());
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                ftpClient.disconnect();
                return Response.status(Response.Status.BAD_REQUEST).entity("connection failed error (alias url).  \n").build();    
            }
            ftpClient.enterLocalPassiveMode();

            ftpClient.login(user.getIdent(), user.getPassword());
            replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                ftpClient.disconnect();
                return Response.status(Response.Status.BAD_REQUEST).entity("connection failed error (user,password).  \n").build();    
            }
        	}
        catch(IOException e){
        	e.printStackTrace();
        }
		Main.Connected_User();
		Main.setFTPClient(ftpClient);
        return Response.status(Response.Status.ACCEPTED).entity("connection succeed.  \n").build();
        }

'''

cette méthode crée un FTPClient et l'associer à notre variable ftpClient.
puis essaye de connecté se ftpclient en utilisant le user et l'alias passé en paramètre les erreurs d'autentification sont gérées dans la réponse. les exceptions sont gérées par le try-catch.
si la connection est effectué alors boolan user_Connected de main est transformé en true par la méthode Connected_User() la méthode setFTPClient() affecte notre FTPClient à l'FtpClient du Main puis ("connection succeed.") est retourné dans la réponse


d)User.User.java


'''

    /**
     * equal methode to compare an instance of User to the current instance
     */
	public boolean equals(User user) {
		if (user.getIdent().equals(this.ident) && user.getPassword().equals(this.password)) {
			return true;
		}
		return false;
	}

'''

cette méthode sert à comparer deux instance de type User.
elle est  appelé par une instance pour savoir si elle est égale à une autre instance que la méthode prend en paramètre.



e)resources.ServerConnection.java


    /**
     * GET: http://localhost:8080/FlopBox/server/list/cdimage
     * function that display the composent of a special directory passed in parametre
     * @param path String
     * @return Response
     */
    @GET
    @Path("/list/{path}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response list(@PathParam("path") String path) throws IOException {
    	boolean connected=Main.getUserConnected(); 
    	if (!connected) {
            return Response.status(Response.Status.BAD_REQUEST).entity("you are disconnected ! try to connect first  \n").build();
    	}    	
    	FTPFile[] file =Main.getFTPClient().listFiles("/"+path);
    	 
    	if (file==null) {
             return Response.status(Response.Status.BAD_REQUEST).entity("empty file  \n").build();

    	}
    	String res="";
    	for (FTPFile f : file) {
    		if (f.isDirectory()) {
    			res = res + "d  ";
    		}
    		else {
    			res = res +"f  ";
    		}
    		res = res + "    name: "+ f.getName();
    		res = res + "    size: "+ f.getSize();
    		res = res + "  \n";
    	}
    	
        return Response.status(Response.Status.OK).entity(res).build();

    }
    
la méthode "list" liste les dossier et fichier d'un répertoir.
pour commencé la méthode vérifier que l'utilisateur est connecté. puis récupére la liste de ses sous_élement(dossier et fichier).
puis parcour cette liste pour afficher des information sur ces sous élement.


## 4-UML ->  racine du projet (Diagram.png) 
