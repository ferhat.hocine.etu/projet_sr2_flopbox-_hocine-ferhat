package resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import Alias.Alias;
import FlopBox.Main;
import User.User;

/**
 * Root resource (exposed at "listServers" path)
 */
@Path("listServers")
public class Servers {

    /**
     * URL http://localhost:8080/FlopBox/listServers/servers
     * function that return the list of servers
     * @return Response
     */
    @GET
    @Path("/servers")
    public Response getServers(){
    	String res="";
    	for (int i=0; i<Main.list_alias.size();i++) {
    		res= res + Main.list_alias.get(i).getUrl()+ " ,";
    	}
    	if (res.equals("")) {
            return Response.status(Response.Status.OK).entity("the list of servers is empty   \n").build();
    	}
    	res =res.substring(0, res.length()-1);
    	res = "Server's list = [ "+res+" ]";
        return Response.status(Response.Status.OK).entity(res+"  \n").build();
    }
    
    /**
     * URL http://localhost:8080/FlopBox/listServers/getServer?alias=ftp
     * function that return the server associated to an alias in the list of servers
     * @param alias
     * @return Response
     */
    @GET
    @Path("/getServer")
    public Response getServer(@QueryParam("alias") String alias){
    	if((alias== null)) {
        	return Response.status(Response.Status.BAD_REQUEST).entity("enter the parameter alias  \n").build();
    	}
    	for (int i=0; i<Main.list_alias.size();i++) {
    		if(Main.list_alias.get(i).getAlis().equals(alias)){
    	        return Response.status(Response.Status.OK).entity("Server : "+Main.list_alias.get(i).getUrl()+"  \n").build();
    		}
    	}
        return Response.status(Response.Status.BAD_REQUEST).entity("Server not Found  \n").build();
    }
    
    /**
     * URL http://localhost:8080/FlopBox/listServers/add?alias=ftp2&url=www.free.fr
     * function that add a new server to the list of servers
     * @param alias
     * @param server
     * @return Response
     */
    @POST
    @Path("/add")
    public Response AddServers(@QueryParam("alias") String alias, @QueryParam("url") String url){
    	if((alias== null)|| (url== null)) {
        	return Response.status(Response.Status.BAD_REQUEST).entity("enter the two parameters  \n").build();
    	}
    	Alias al=new Alias(alias,url);
    	if (Main.list_alias.contains(al)) {
        	return Response.status(Response.Status.BAD_REQUEST).entity("Server already existe in the list of servers  \n").build();
    	}
    	Main.list_alias.add(al);
        return Response.status(Response.Status.OK).entity("Server added successfuly  \n").build();
    }
    
    /**
     * URL http://localhost:8080/FlopBox/listServers/delete?alias=ftp2
     * function that remove a server from the list of servers
     * @param alias
     * @return Response
     */
    @DELETE
    @Path("/delete")
    public Response deleteServers(@QueryParam("alias") String alias){
    	if((alias== null)) {
        	return Response.status(Response.Status.BAD_REQUEST).entity("enter the parameter alias  \n").build();
    	}
    	for (int i=0; i<Main.list_alias.size();i++) {
    		if(Main.list_alias.get(i).getAlis().equals(alias)){
    			Main.list_alias.remove(i);
    	        return Response.status(Response.Status.OK).entity("Server removed successfuly  \n").build();
    		}
    	}
        return Response.status(Response.Status.BAD_REQUEST).entity("Server not Found  \n").build();
    }
    
    /**
     * URL http://localhost:8080/FlopBox/listServers/Put?alias=ftp2&url_update=www.free2.fr
     * function that update a specifique server from the list of servers
     * @param alias
     * @param server
     * @return Response
     */
    @PUT
    @Path("/Put")
    public Response updateServers(@QueryParam("alias") String alias, @QueryParam("url_update") String url_update){
    	if((alias== null)|| (url_update== null)) {
        	return Response.status(Response.Status.BAD_REQUEST).entity("enter the two parameters  \n").build();
    	}
    	for (int i=0; i<Main.list_alias.size();i++) {
        		if(Main.list_alias.get(i).getAlis().equals(alias)){
        			Main.list_alias.get(i).setUrl(url_update);
        	        return Response.status(Response.Status.OK).entity("Server updated successfuly  \n").build();
        		}
        }
    	return Response.status(Response.Status.BAD_REQUEST).entity("Server doesn't existe in the list of servers  \n").build();   		
    	}
    
    
}
