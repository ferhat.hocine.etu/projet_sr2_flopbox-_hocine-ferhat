package resources;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import Alias.Alias;
import FlopBox.Main;
import User.User;

/**
 * Root resource (exposed at "server" path)
 */
@Path("server")
public class ServerConnection {
    private FTPClient ftpClient;

    /**
     * POST: http://localhost:8080/FlopBox/server?alias=ftp
     * function that call the function connect to connect the server passed in parametre using the parameters
     * @param alias
     * @param url
     * @param user
     * @param password
     * @return Response
     * @throws IOException 
     */
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Response connectServer(@QueryParam("alias") String alias, @HeaderParam("url") String url,
    		@HeaderParam("user") String user, @HeaderParam("password") String password) throws IOException{
        if ((user== null ) || (password== null)|| (alias== null)|| (url== null)) {
        	return Response.status(Response.Status.BAD_REQUEST).entity("enter the four parameters  \n").build();
        }
        User n_User= new User(user,password);
        Alias al = new Alias(alias,url);
    	if(Main.isSavedUser(n_User)){
    		if(Main.isSavedAlias(al)) {
    			Response r=connect(n_User,al);
    			return r;
    		}
            return Response.status(Response.Status.BAD_REQUEST).entity("server doesn't existe  \n").build();
    	}
        return Response.status(Response.Status.BAD_REQUEST).entity("User or password error  \n").build();
    }
    
    /**
     * function that connect to server
     * @param User user
     * @param Alias al
     * @return Response
     * @throws IOException
     */
    public Response connect(User user,Alias al ) throws IOException {

        ftpClient = new FTPClient();

        try {
            ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out), true));
            ftpClient.connect(al.getUrl());
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                ftpClient.disconnect();
                return Response.status(Response.Status.BAD_REQUEST).entity("connection failed error (alias url).  \n").build();    
            }
            ftpClient.enterLocalPassiveMode();

            ftpClient.login(user.getIdent(), user.getPassword());
            replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                ftpClient.disconnect();
                return Response.status(Response.Status.BAD_REQUEST).entity("connection failed error (user,password).  \n").build();    
            }
        	}
        catch(IOException e){
        	e.printStackTrace();
        }
		Main.Connected_User();
		Main.setFTPClient(ftpClient);
        return Response.status(Response.Status.ACCEPTED).entity("connection succeed.  \n").build();
        }
    
    
    
    
    /**
     * GET: http://localhost:8080/FlopBox/server/list/cdimage
     * function that display the composent of a special directory passed in parametre
     * @param path String
     * @return Response
     */
    @GET
    @Path("/list/{path}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response list(@PathParam("path") String path) throws IOException {
    	boolean connected=Main.getUserConnected(); 
    	if (!connected) {
            return Response.status(Response.Status.BAD_REQUEST).entity("you are disconnected ! try to connect first  \n").build();
    	}    	
    	FTPFile[] file =Main.getFTPClient().listFiles("/"+path);
    	 
    	if (file==null) {
             return Response.status(Response.Status.BAD_REQUEST).entity("empty file  \n").build();

    	}
    	String res="";
    	for (FTPFile f : file) {
    		if (f.isDirectory()) {
    			res = res + "d  ";
    		}
    		else {
    			res = res +"f  ";
    		}
    		res = res + "    name: "+ f.getName();
    		res = res + "    size: "+ f.getSize();
    		res = res + "  \n";
    	}
    	
        return Response.status(Response.Status.OK).entity(res).build();

    }
    
    
    
}
