package Alias;

import User.User;
/**
 * Class to represent server with an alias to call it 
 */
public class Alias {
	private String alis;
	private String url;
    /**
     * Constructor
     */
	public Alias(String alis, String url) {
		this.alis = alis;
		this.url = url;
	}
	
    /**
     * getter
     */
	public String getAlis() {
		return this.alis;
	}
    /**
     * getter
     */
	public String getUrl() {
		return this.url;
	}
    /**
     * setter
     */
	public void setAlis(String alis1) {
		this.alis = alis1;
	}
    /**
     * setter
     */
	public void setUrl(String url1) {
		this.url = url1;
	}
    /**
     * equal methode to compare an instance of alias to the current instance
     */
	public boolean equals(Alias al) {
		if (al.getAlis().equals(this.alis) && al.getUrl().equals(this.url)) {
			return true;
		}
		return false;
	}
}