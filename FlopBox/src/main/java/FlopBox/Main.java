package FlopBox;

import org.apache.commons.net.ftp.FTPClient;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import Alias.Alias;
import User.User;

import java.awt.List;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;

/**
 * Main class.
 *
 */
public class Main {
	public static ArrayList<User> list_users = new ArrayList<User>();
	public static ArrayList<Alias> list_alias = new ArrayList<Alias>();
	public static boolean user_Connected=false;
    private static FTPClient ftpClient;

    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8080/FlopBox/";

    public static boolean isSavedUser(User user) {
		for (int i=0; i<list_users.size();i++) {
			if(user.equals(list_users.get(i))) {
				return true;
			}
		}
		return false;
    }
    public static boolean isSavedAlias(Alias alias) {
		for (int i=0; i<list_alias.size();i++) {
			if(alias.equals(list_alias.get(i))) {
				return true;
			}
		}
		return false;
    }
    
    public static void Connected_User() {
    	user_Connected=true;
    }
    public static boolean getUserConnected() {
    	return user_Connected;
    }
    public static FTPClient getFTPClient() {
    	return ftpClient;
    }
    public static void setFTPClient(FTPClient ftp) {
    	ftpClient=ftp;
    }
    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in FlopBox package
        final ResourceConfig rc = new ResourceConfig().packages("resources");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

    	User user=new User("anonymous","anonymous");
    	list_users.add(user);
    	Alias alias=new Alias("ftp","ftp.ubuntu.com");
    	list_alias.add(alias);

    	
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();

        server.stop();
    }
}

